---
title: "Version 1.1, CLI tool available!"
date: 2020-09-11T15:48:10+02:00
tags: ["release"]
draft: false
---

This new version brings you the latest part to fully automate your doc generation, the Antidoc CLI tool.
With it, you will add the document generation as part of your CI/CD setup.

![Antidoc CLI Tool in Action](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/CLI-for-Antidoc.gif)

This version is also the opportunity to continue to improve Antidoc. Among the different modification we made, we want to point you the following:

* Extended character set is now supported with the last version of [Asciidoc toolkit for LabVIEW](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/posts/2020-9-9-new-maintainance-release-v-1.2/)
* Tool GUI displays a progress bar that gives you information on the task running during the document generation process
* There is an option to ask Antidoc to parse libraries contained in the project dependencies

![Antidoc GUI Tool in Action](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/Antidoc-GUI-Progress-Bar.gif)
<!--more-->
## Antidoc v1.1.0

The package is directly available through [VI Package Manager](https://www.vipm.io/package/wovalab_lib_antidoc/).

### Release note

#### New features
* Add a progress bar in the tool GUI -->  [#66](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/66)
* Add an option to scan libraries in the Dependencies -->  [#65](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/65)

#### Changes
* File name extension handling --> [#67](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/67)
NOTE: This change may generate bug for code relying on the public API
* Check if lvproj path is correctly set -->  [#69](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/69)
* Add default title if not set by the user --> [#67](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/68)

#### Bug fix
* Misspelling in the document generated --> [#61](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/61)
* UTF-8 encoding --> [#59](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/59)

## Antidoc CLI v1.0.0

CLI tool is available through its own package.

The package is directly available through [VI Package Manager](https://www.vipm.io/package/wovalab_lib_antidoc_cli/).

It relies on the great [G CLI](https://www.vipm.io/package/wiresmith_technology_lib_g_cli/) toolkit developed by [James McNally](https://github.com/JamesMc86).