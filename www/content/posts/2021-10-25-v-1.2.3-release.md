---
title: "Version 1.2.3, maintainance release"
date: 2021-12-16T10:02:10+02:00
tags: ["release","DQMH"]
draft: false
---

This new version is fixing a bug that can prevent you to get the documentation generated.
See the release note for more details.

## Antidoc v1.2.3

The package is directly available through [VI Package Manager](https://www.vipm.io/package/wovalab_lib_antidoc/).

### Release note

#### Bug fix
* Error 1099 when parsing project --> [#162](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/162)
* Too much "…" in the menu item --> [#165](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/165)